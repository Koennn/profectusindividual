package me.koenn.profectusi.quickshop;

import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import me.koenn.core.misc.ItemHelper;
import me.koenn.core.misc.LoreHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class RepairGui extends Gui {

    protected RepairGui(Player player) {
        //Call the Gui constructor.
        super(player, "Repairs", 27);

        //Add all Repair items.
        this.setOption(10, new Option(ItemHelper.makeItemStack(
                Material.DIAMOND_PICKAXE, 1, (short) 0,
                ChatColor.YELLOW + "Pickaxe Repair",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $100")
        ), () -> {
        }));
        this.setOption(12, new Option(ItemHelper.makeItemStack(
                Material.DIAMOND_AXE, 1, (short) 0,
                ChatColor.YELLOW + "Axe Repair",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $100")
        ), () -> {
        }));
        this.setOption(14, new Option(ItemHelper.makeItemStack(
                Material.DIAMOND_SPADE, 1, (short) 0,
                ChatColor.YELLOW + "Shovel Repair",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $100")
        ), () -> {
        }));
        this.setOption(16, new Option(ItemHelper.makeItemStack(
                Material.BARRIER, 1, (short) 0,
                ChatColor.RED + "Auto-Repair", null
        ), () -> {
        }));

        //Fill the rest of the Gui with gray glass panes.
        for (int i = 0; i < 27; i++) {
            if (this.getGui().getItem(i) == null) {
                this.setOption(i, new Option(ItemHelper.makeItemStack(
                        Material.STAINED_GLASS_PANE, 1, (short) 7, "", null
                ), () -> {

                }));
            }
        }
    }
}
