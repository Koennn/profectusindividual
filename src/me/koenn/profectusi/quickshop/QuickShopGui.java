package me.koenn.profectusi.quickshop;

import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import me.koenn.core.misc.ItemHelper;
import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.references.Items;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class QuickShopGui extends Gui {

    public QuickShopGui(Player player) {
        //Call the Gui constructor.
        super(player, "QuickShop");

        //Add the Ammo icon item which opens the AmmoGui.
        this.setOption(2, new Option(Items.AMMO_ICON, () -> {
            Gui gui = new AmmoGui(player);
            Gui.registerGui(gui, ProfectusIndividual.getInstance());
            gui.open();
        }));

        //Add the Repair icon item which opens the RepairGui.
        this.setOption(6, new Option(Items.REPAIR_ICON, () -> {
            Gui gui = new RepairGui(player);
            Gui.registerGui(gui, ProfectusIndividual.getInstance());
            gui.open();
        }));

        //Fill the rest of the Gui with gray glass panes.
        for (int i = 0; i < 9; i++) {
            if (this.getGui().getItem(i) == null) {
                this.setOption(i, new Option(ItemHelper.makeItemStack(
                        Material.STAINED_GLASS_PANE, 1, (short) 7, "", null
                ), () -> {

                }));
            }
        }
    }
}
