package me.koenn.profectusi.quickshop;

import me.koenn.core.gui.Gui;
import me.koenn.core.gui.Option;
import me.koenn.core.misc.ItemHelper;
import me.koenn.core.misc.LoreHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class AmmoGui extends Gui {

    public AmmoGui(Player player) {
        //Call the Gui constructor.
        super(player, "Ammo", 27);

        //Add all Ammo items
        this.setOption(1, new Option(ItemHelper.makeItemStack(
                Material.ARROW, 64, (short) 0,
                ChatColor.YELLOW + "Pistol Ammo",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $100")
        ), () -> {
        }));
        this.setOption(3, new Option(ItemHelper.makeItemStack(
                Material.ARROW, 64, (short) 0,
                ChatColor.YELLOW + "Shotgun Ammo",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $100")
        ), () -> {
        }));
        this.setOption(19, new Option(ItemHelper.makeItemStack(
                Material.ARROW, 32, (short) 0,
                ChatColor.YELLOW + "Pistol Ammo",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $50")
        ), () -> {
        }));
        this.setOption(21, new Option(ItemHelper.makeItemStack(
                Material.ARROW, 32, (short) 0,
                ChatColor.YELLOW + "Shotgun Ammo",
                LoreHelper.makeLore(ChatColor.GREEN + "Price: $50")
        ), () -> {
        }));
        this.setOption(14, new Option(ItemHelper.makeItemStack(
                Material.BARRIER, 1, (short) 0,
                ChatColor.RED + "Pistol Auto Ammo", null
        ), () -> {
        }));
        this.setOption(16, new Option(ItemHelper.makeItemStack(
                Material.BARRIER, 1, (short) 0,
                ChatColor.RED + "Shotgun Auto Ammo", null
        ), () -> {
        }));

        //Fill the rest of the Gui with gray glass panes.
        for (int i = 0; i < 27; i++) {
            if (this.getGui().getItem(i) == null) {
                this.setOption(i, new Option(ItemHelper.makeItemStack(
                        Material.STAINED_GLASS_PANE, 1, (short) 7, "", null
                ), () -> {

                }));
            }
        }
    }
}
