package me.koenn.profectusi.saturation;

import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.util.Logger;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class SaturationSystem {

    private static final HashMap<Material, Float> saturationItems = new HashMap<>();
    public static int taskId;

    public static boolean givesSaturation(ItemStack item) {
        return saturationItems.containsKey(item.getType());
    }

    public static int getSaturation(ItemStack item) {
        return (int) (saturationItems.get(item.getType()) * 2);
    }

    public static void giveSaturation(Player player, int amount) {
        float saturation = player.getSaturation();
        if (saturation < 250) {
            if (saturation + amount > 250) {
                player.setSaturation(250);
            } else {
                player.setSaturation(saturation + amount);
            }
        }
    }

    public static void takeSaturation(Player player, int amount) {
        float saturation = player.getSaturation();
        if (saturation > 0) {
            if (saturation + amount < 0) {
                player.setSaturation(0);
            } else {
                player.setSaturation(saturation - amount);
            }
        }
    }

    public static void loadSaturationItems() {
        saturationItems.clear();
        List<String> items = ProfectusIndividual.getConfigManager().getList("saturationItems", "saturationSystem");
        for (String item : items) {
            String[] split = item.split(" ");
            try {
                saturationItems.put(Material.valueOf(split[0].toUpperCase()), Float.parseFloat(split[1]));
            } catch (NumberFormatException ex) {
                Logger.error("Invalid number \'" + split[1] + "\' in saturationItems!");
            } catch (ArrayIndexOutOfBoundsException ex) {
                Logger.error("Invalid format \'" + item + "\' in saturationItems!");
            } catch (Exception ex) {
                Logger.error("Unknown item \'" + split[0] + "\' in saturationItems!");
            }
        }
    }

    public static void startSaturationUpdater() {
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(ProfectusIndividual.getInstance(), () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getSaturation() < 0) {
                    player.setSaturation(0);
                }

                player.setFoodLevel(20);

                String message = ChatColor.DARK_RED + "" + ChatColor.BOLD + "                     \u2726 " + Math.round(player.getSaturation());

                IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
                PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(bar);
            }
        }, 10, 20);


    }
}
