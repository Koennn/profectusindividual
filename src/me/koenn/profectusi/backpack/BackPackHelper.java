package me.koenn.profectusi.backpack;

import me.koenn.profectusi.ProfectusIndividual;
import org.json.simple.JSONObject;

import java.util.UUID;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class BackPackHelper {

    /**
     * Get a BackPack from the data file.
     *
     * @param owner UUID of BackPack owner
     * @return BackPack object instance
     */
    public static BackPack getBackPack(UUID owner) {
        try {
            //Get the BackPack JSONObject from the data file.
            JSONObject backPackObject = (JSONObject) ((JSONObject) ProfectusIndividual.getDataManager().getFromBody("backpacks")).get(owner + "_backpack");

            //Load the int[] from the file.
            int[] items = new int[10];
            String[] itemsString = ((String) backPackObject.get("items")).replace("[", "").replace("]", "").split(", ");
            for (int i = 0; i < items.length; i++) {
                items[i] = Integer.parseInt(itemsString[i]);
            }

            //Return a new BackPack with the owner and the int[]
            return new BackPack(owner, items);
        } catch (Exception ex) {

            //If any error occurs, return null.
            return null;
        }
    }
}
