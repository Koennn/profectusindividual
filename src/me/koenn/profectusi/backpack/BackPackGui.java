package me.koenn.profectusi.backpack;

import me.koenn.core.misc.ItemHelper;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class BackPackGui {

    private final Inventory gui;

    public BackPackGui(BackPack backPack) {
        //Create the gui Inventory object.
        gui = Bukkit.createInventory(null, 27, "BackPack");

        //Make sure the BackPack exists.
        if (backPack == null) {
            throw new NullPointerException("BackPack cannot be null");
        }

        //Create the BackPack items.
        ItemStack[] items = new ItemStack[]{
                ItemHelper.makeItemStack(Material.LOG, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[0] + "x Logs", null),
                ItemHelper.makeItemStack(Material.COBBLESTONE, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[1] + "x Cobblestone", null),
                ItemHelper.makeItemStack(Material.DIRT, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[2] + "x Dirt", null),
                ItemHelper.makeItemStack(Material.SAND, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[3] + "x Sand", null),
                ItemHelper.makeItemStack(Material.GRAVEL, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[4] + "x Gravel", null),
                ItemHelper.makeItemStack(Material.DIAMOND, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[5] + "x Diamond", null),
                ItemHelper.makeItemStack(Material.IRON_INGOT, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[6] + "x Iron", null),
                ItemHelper.makeItemStack(Material.GOLD_INGOT, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[7] + "x Gold", null),
                ItemHelper.makeItemStack(Material.INK_SACK, 1, (short) 4, ChatColor.GOLD.toString() + backPack.getItems()[8] + "x Lapis", null),
                ItemHelper.makeItemStack(Material.COAL, 1, (short) 0, ChatColor.GOLD.toString() + backPack.getItems()[9] + "x Coal", null)
        };

        //Set the items in the right slots.
        for (int i = 0; i < items.length; i++) {
            int slot = i;
            if (i <= 4) {
                slot *= 2;
            } else {
                slot *= 2;
                slot += 8;
            }

            gui.setItem(slot, items[i]);
        }

        //Fill the rest of the BackPack with glass panes.
        for (int i = 0; i < 26; i++) {
            if (gui.getItem(i) == null || gui.getItem(i).getType().equals(Material.AIR)) {
                gui.setItem(i, ItemHelper.makeItemStack(
                        Material.STAINED_GLASS_PANE, 1, (short) 7, "", null
                ));
            }
        }
    }

    public void open(Player player) {
        player.openInventory(gui);
    }
}
