package me.koenn.profectusi.backpack;

import me.koenn.core.registry.Registry;
import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.references.Items;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.UUID;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class BackPack {

    public static final Registry<BackPack> backPackRegistry = new Registry<>(backPack -> backPack.getOwner().toString());
    public static int taskId;

    private final int[] items;
    private final UUID owner;

    public BackPack(UUID owner) {
        this(owner, new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
    }

    public BackPack(UUID owner, int[] items) {
        backPackRegistry.register(this);
        this.items = items;
        this.owner = owner;
    }

    public static void startBackPackTask() {
        //Start the BackPack updater task.
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(ProfectusIndividual.getInstance(), () -> {

            //Loop over all online players.
            for (Player player : Bukkit.getOnlinePlayers()) {

                //Get the player's BackPack from the registry.
                BackPack backPack = BackPack.backPackRegistry.get(player.getUniqueId().toString());

                //Loop over all items in the player's inventory.
                for (ItemStack item : player.getInventory().getContents()) {

                    //Make sure the item is not null.
                    if (item == null) {
                        continue;
                    }

                    //Loop over all BackPack items.
                    for (int i = 0; i < Items.BACKPACK_ITEMS.length; i++) {

                        //Check if the item is the same type as the BackPack item.
                        if (Items.BACKPACK_ITEMS[i].equals(item.getType())) {

                            //Add the amount of items to the amount in the backpack.
                            backPack.getItems()[i] += item.getAmount();

                            //Remove the items from the player's inventory.
                            player.getInventory().remove(item);
                        }
                    }
                }

                //Update the player's inventory.
                player.updateInventory();
            }
        }, 0, 2 * 20);
    }

    public int[] getItems() {
        return items;
    }

    public UUID getOwner() {
        return owner;
    }

    @SuppressWarnings("deprecation")
    public void click(InventoryClickEvent event, Player player) {
        //Make sure the clicked item exists.
        if (event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) {
            return;
        }
        //Make sure the cursor item exists.
        if (event.getCursor() == null || event.getCursor().getType().equals(Material.AIR)) {
            return;
        }
        //Make sure the clicked item and the cursor item are the same.
        if (!event.getCurrentItem().getType().equals(event.getCursor().getType())) {
            return;
        }

        //Get the BackPack index of the item.
        int index = event.getSlot();
        if (event.getSlot() <= 8) {
            index /= 2;
        } else {
            index -= 8;
            index /= 2;
        }

        //Add the amount of items to the BackPack.
        this.items[index] += event.getCursor().getAmount();

        //Set the cursor to air.
        event.setCursor(new ItemStack(Material.AIR));

        //Reopen the BackPackGui to update the amounts.
        new BackPackGui(this).open(player);
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("items", Arrays.toString(this.items));
        return jsonObject;
    }
}
