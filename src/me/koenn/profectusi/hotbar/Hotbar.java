package me.koenn.profectusi.hotbar;

import me.koenn.core.misc.ItemHelper;
import me.koenn.core.registry.Registry;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.UUID;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Hotbar {

    public static final Registry<Hotbar> hotbarRegistry = new Registry<>(hotbar -> hotbar.owner + "_" + hotbar.type.getName());

    private final HotbarType type;
    private final ItemStack[] items;
    private final UUID owner;

    public Hotbar(HotbarType type, ItemStack[] items, UUID owner) {
        this.type = type;
        this.items = items;
        this.owner = owner;
    }

    public HotbarType getType() {
        return this.type;
    }

    public ItemStack[] getItems() {
        return this.items;
    }

    public UUID getOwner() {
        return owner;
    }

    public void set(Player player) {
        for (int i = 0; i < 8; i++) {
            ItemStack item = this.items[i];
            if (item != null) {
                player.getInventory().setItem(i, item);
            } else {
                player.getInventory().setItem(i, new ItemStack(Material.AIR));
            }
        }
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        JSONArray items = new JSONArray();
        for (ItemStack item : this.items) {
            if (item == null) {
                items.add("null");
            } else {
                items.add(ItemHelper.itemToString(item));
            }
        }

        jsonObject.put("name", this.type.getName());
        jsonObject.put("items", items);
        return jsonObject;
    }
}
