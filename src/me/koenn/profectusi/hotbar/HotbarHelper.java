package me.koenn.profectusi.hotbar;

import me.koenn.core.misc.ItemHelper;
import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.backpack.BackPack;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.UUID;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class HotbarHelper {

    /**
     * Id of the autoSaver task.
     */
    public static int taskId;

    /**
     * Get a certain Hotbar from a certain Player.
     *
     * @param uuid Player's uuid
     * @param type Hotbar type
     * @return Hotbar object
     */
    public static Hotbar getHotbar(UUID uuid, HotbarType type) {
        try {
            //Get the Hotbar JSONObject from the data file.
            JSONObject hotbarObject = (JSONObject) ((JSONObject) ProfectusIndividual.getDataManager().getFromBody("hotbars")).get(uuid + "_" + type.getName());

            //Get the items array from the JSONObject.
            JSONArray itemArray = (JSONArray) hotbarObject.get("items");

            //Get the ItemStack[] from the JSONArray.
            ItemStack[] items = new ItemStack[9];
            for (int i = 0; i < 8; i++) {
                if (itemArray.get(i) == null || itemArray.get(i).equals("null")) {
                    items[i] = null;
                } else {
                    items[i] = ItemHelper.stringToItem((String) itemArray.get(i));
                }
            }

            //Return a new Hotbar with the type, items and uuid.
            return new Hotbar(type, items, uuid);
        } catch (Exception ex) {

            //If any error occurs, return null.
            return null;
        }
    }

    /**
     * Start the automatic Hotbar and BackPack saver.
     */
    public static void startAutoSave() {
        //Start the autoSaver task.
        taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(ProfectusIndividual.getInstance(), () -> {

            //Get the hotbars JSON section.
            JSONObject hotbars = (JSONObject) ProfectusIndividual.getDataManager().getFromBody("hotbars");
            if (hotbars == null) {
                hotbars = new JSONObject();
            }

            //Get the backpacks JSON section.
            JSONObject backpacks = (JSONObject) ProfectusIndividual.getDataManager().getFromBody("backpacks");
            if (backpacks == null) {
                backpacks = new JSONObject();
            }

            //Loop over all online players.
            for (Player player : Bukkit.getOnlinePlayers()) {

                //Get the HotbarType that's currently selected.
                String[] split = player.getInventory().getItem(8).getItemMeta().getLore().get(0).split(" ");
                HotbarType type = HotbarType.valueOf(split[split.length - 1].toUpperCase());
                if (type.equals(HotbarType.TOOLBAR)) {
                    type = HotbarType.WEAPONBAR;
                } else {
                    type = HotbarType.TOOLBAR;
                }

                //Make local variables.
                Inventory inventory = player.getInventory();
                Hotbar hotbar = Hotbar.hotbarRegistry.get(player.getUniqueId() + "_" + type.getName());

                //Save their items to the Hotbar
                for (int i = 0; i < 8; i++) {
                    ItemStack itemStack = inventory.getItem(i);
                    hotbar.getItems()[i] = itemStack;
                }
            }

            //Loop over all registered Hotbars.
            for (Hotbar hotbar : Hotbar.hotbarRegistry.getRegisteredObjects()) {

                //Put the Hotbar in the JSONObject.
                hotbars.put(hotbar.getOwner() + "_" + hotbar.getType().getName(), hotbar.toJSON());
            }

            //Loop over all registered BackPacks.
            for (BackPack backPack : BackPack.backPackRegistry.getRegisteredObjects()) {

                //Put the Hotbar in the JSONObject.
                backpacks.put(backPack.getOwner() + "_backpack", backPack.toJSON());
            }

            //Save the JSONObject with the Hotbars to the file.
            ProfectusIndividual.getDataManager().setInBody("hotbars", hotbars);

            //Save the JSONObject with the BackPacks to the file.
            ProfectusIndividual.getDataManager().setInBody("backpacks", backpacks);

        }, 10, 60 * 20);
    }
}
