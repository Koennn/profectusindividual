package me.koenn.profectusi.hotbar;

import me.koenn.core.misc.ColorHelper;
import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.references.Items;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public enum HotbarType {

    TOOLBAR("ToolBar", new ItemStack[]{
            null,
            null,
            null,
            null,
            Items.QUICKSHOP,
            null,
            Items.BACKPACK,
            null,
            Items.BAR_SWITCH
    }),

    WEAPONBAR("WeaponBar", new ItemStack[]{
            null,
            null,
            null,
            null,
            Items.QUICKSHOP,
            null,
            Items.BACKPACK,
            null,
            Items.BAR_SWITCH2
    });

    private final String name;
    private final ItemStack[] items;

    HotbarType(String name, ItemStack[] items) {
        this.name = name;
        this.items = items;

        List<String> starterItems = ProfectusIndividual.getConfigManager().getList(this.name().toLowerCase(), "startingItems");
        for (int i = 0; i < starterItems.size(); i++) {
            String item = starterItems.get(i);
            String[] components = item.split(" ");
            Material material = Material.valueOf(components[0].toUpperCase());
            ItemStack itemStack = new ItemStack(material);
            ItemMeta meta = itemStack.getItemMeta();
            if (components.length > 2) {
                if (components[1].equalsIgnoreCase("named")) {
                    meta.setDisplayName(ColorHelper.readColor(components[2]).replace("_", " "));
                }
            }
            itemStack.setItemMeta(meta);
            this.items[i] = itemStack;
        }
    }

    public String getName() {
        return name;
    }

    public ItemStack[] getItems() {
        return items;
    }
}
