package me.koenn.profectusi.references;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class Fields {

    /**
     * Experience player data field name.
     */
    public static final String EXPERIENCE = "experience";

    /**
     * Rank player data field name.
     */
    public static final String RANK = "rank";
}
