package me.koenn.profectusi.references;

import me.koenn.core.misc.ItemHelper;
import me.koenn.core.misc.LoreHelper;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class Items {

    /**
     * BackPack item for in the player's inventory.
     */
    public static final ItemStack BACKPACK = ItemHelper.makeItemStack(
            Material.CHEST, 1, (short) 0,
            ChatColor.WHITE + "BackPack",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to open!")
    );

    /**
     * HotbarSwitch item for in the player's inventory.
     * Switches to the WeaponBar.
     */
    public static final ItemStack BAR_SWITCH = ItemHelper.makeItemStack(
            Material.WATCH, 1, (short) 0,
            ChatColor.WHITE + "Swap Hotbar",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to switch to your WeaponBar")
    );

    /**
     * HotbarSwitch item for in the player's inventory.
     * Switches to the ToolBar
     */
    public static final ItemStack BAR_SWITCH2 = ItemHelper.makeItemStack(
            Material.WATCH, 1, (short) 0,
            ChatColor.WHITE + "Swap Hotbar",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to switch to your ToolBar")
    );

    /**
     * QuickShop item for in the player's inventory.
     */
    public static final ItemStack QUICKSHOP = ItemHelper.makeItemStack(
            Material.EMERALD, 1, (short) 0,
            ChatColor.GREEN + "Quick Shop",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to open!")
    );

    /**
     * All allowed items in the BackPack.
     */
    public static final Material[] BACKPACK_ITEMS = new Material[]{
            Material.LOG, Material.COBBLESTONE, Material.DIRT, Material.SAND, Material.GRAVEL,
            Material.DIAMOND, Material.IRON_INGOT, Material.GOLD_INGOT, Material.INK_SACK, Material.COAL
    };

    /**
     * Ammo icon item for in the QuickShop gui.
     */
    public static final ItemStack AMMO_ICON = ItemHelper.makeItemStack(
            Material.ARROW, 1, (short) 0,
            ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Ammo Shop",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to open!")
    );

    /**
     * Repair icon item for in the QuickShop gui.
     */
    public static final ItemStack REPAIR_ICON = ItemHelper.makeItemStack(
            Material.ANVIL, 1, (short) 0,
            ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Repair Shop",
            LoreHelper.makeLore(ChatColor.YELLOW + "Click to open!")
    );
}
