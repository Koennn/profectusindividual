package me.koenn.profectusi;

import me.koenn.core.config.ConfigManager;
import me.koenn.core.data.JSONManager;
import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.hotbar.HotbarHelper;
import me.koenn.profectusi.listeners.*;
import me.koenn.profectusi.saturation.SaturationSystem;
import me.koenn.profectusi.util.Logger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * ProfectusIndividual plugin version 1.0.
 * <p>
 * This plugin is made for the Profectus project, and is not meant
 * for any other use outside of this project.
 * <p>
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class ProfectusIndividual extends JavaPlugin {

    /**
     * Static instance of this plugin class.
     */
    private static Plugin instance;

    /**
     * JSON data manager for the plugin.
     */
    private static JSONManager dataManager;

    /**
     * ConfigManager for the plugin.
     */
    private static ConfigManager configManager;

    /**
     * Get the instance of this class.
     *
     * @return Plugin instance
     */
    public static Plugin getInstance() {
        return instance;
    }

    /**
     * Get JSON data manager.
     *
     * @return JSONManager object
     */
    public static JSONManager getDataManager() {
        return dataManager;
    }

    /**
     * Get the ConfigManager.
     *
     * @return ConfigManager object
     */
    public static ConfigManager getConfigManager() {
        return configManager;
    }

    /**
     * Plugin enable method.
     * Gets called when the plugin enables
     */
    @Override
    public void onEnable() {
        //Set static variables.
        instance = this;

        //Send credit message.
        Logger.info("All credits for this plugin go to Koenn");

        //Check the Bukkit version and disable the plugin if its invalid.
        if (!checkVersion()) {
            Logger.error("This plugin is only compatible with minecraft 1.8");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        //Register Bukkit event Listeners.
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDropItemListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerPickupItemListener(), this);
        Bukkit.getPluginManager().registerEvents(new ItemInteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(), this);

        //Create the JSON data manager.
        dataManager = new JSONManager(this, "data.json");

        //Create the ConfigManager.
        configManager = new ConfigManager(this);

        //Load the saturationItems.
        SaturationSystem.loadSaturationItems();

        //Start the saturation display.
        SaturationSystem.startSaturationUpdater();

        //Start the Hotbar and BackPack auto-saver.
        HotbarHelper.startAutoSave();

        //Start the BackPack updater task.
        BackPack.startBackPackTask();
    }

    /**
     * Get the current Bukkit version of the server and check if its valid.
     */
    public boolean checkVersion() {
        //Check the version and set the version variable.
        String versionString = Bukkit.getServer().getBukkitVersion();
        String version;
        if (versionString.contains("1.8")) {
            version = "1.8";
        } else if (versionString.contains("1.9")) {
            return false;
        } else if (versionString.contains("1.10")) {
            return false;
        } else {
            return false;
        }
        Logger.info("Loading plugin for minecraft version " + version + "!");
        return true;
    }

    /**
     * Plugin disable method.
     * Gets called when the plugin disables.
     */
    @Override
    public void onDisable() {
        //Send credit message.
        Logger.info("All credits for this plugin go to Koenn");

        //Stop all repeating tasks.
        Bukkit.getScheduler().cancelTask(HotbarHelper.taskId);
        Bukkit.getScheduler().cancelTask(BackPack.taskId);
        Bukkit.getScheduler().cancelTask(SaturationSystem.taskId);
    }
}