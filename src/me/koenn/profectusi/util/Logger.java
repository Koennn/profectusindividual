package me.koenn.profectusi.util;

import me.koenn.profectusi.ProfectusIndividual;
import org.bukkit.Bukkit;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class Logger {

    public static final boolean DEBUG = true;

    public static void info(String message) {
        Bukkit.getLogger().info(String.format("[%s] %s", ProfectusIndividual.getInstance().getName(), message));
    }

    public static void debug(String message) {
        if (DEBUG) {
            Bukkit.getLogger().info(String.format("[%s] [DEBUG] %s", ProfectusIndividual.getInstance().getName(), message));
        }
    }

    public static void error(String message) {
        String formattedMessage = String.format("[%s] %s", ProfectusIndividual.getInstance().getName(), message);
        String line = "";
        for (int i = 0; i < formattedMessage.length(); i++) {
            line += "#";
        }

        Bukkit.getLogger().severe(line);
        Bukkit.getLogger().severe(formattedMessage);
        Bukkit.getLogger().severe(line);
    }
}
