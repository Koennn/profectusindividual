package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.saturation.SaturationSystem;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public class PlayerMoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        //Make local variables.
        CPlayer player = CPlayerRegistry.getCPlayer(event.getPlayer().getUniqueId());
        Location from = event.getFrom();
        Location to = event.getTo();
        int multiplier = 1;
        int steps;
        try {
            steps = Integer.parseInt(player.get("steps"));
        } catch (Exception ex) {
            steps = 0;
        }

        if (to.getBlockY() > from.getBlockY()) {
            SaturationSystem.takeSaturation(player.getPlayer(), 2);
        } else if (to.getBlockX() != from.getBlockX() || to.getBlockZ() != from.getBlockZ()) {
            steps++;
            if (player.getPlayer().isSprinting()) {
                multiplier = 2;
            }
            if (steps == (10 / multiplier) || steps > 10) {
                SaturationSystem.takeSaturation(player.getPlayer(), 1);
                steps = 0;
            }
            player.set("steps", String.valueOf(steps));
        }
    }
}