package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.backpack.BackPackHelper;
import me.koenn.profectusi.hotbar.Hotbar;
import me.koenn.profectusi.hotbar.HotbarHelper;
import me.koenn.profectusi.hotbar.HotbarType;
import me.koenn.profectusi.references.Fields;
import me.koenn.profectusi.references.Items;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        //Make the local variables.
        CPlayer player = CPlayerRegistry.getCPlayer(event.getPlayer().getUniqueId());

        //Get the player's BackPack from the registry.
        BackPack backPack = BackPack.backPackRegistry.get(player.getUUID().toString());

        //Check if the BackPack is null.
        if (backPack == null) {

            //Get the BackPack from the file.
            backPack = BackPackHelper.getBackPack(player.getUUID());

            //Check if the BackPack is null.
            if (backPack == null) {

                //Create a new BackPack.
                new BackPack(player.getUUID());
            }
        }

        //Loop over all HotbarTypes.
        for (HotbarType type : HotbarType.values()) {

            //Get the Hotbar from the registry.
            Hotbar hotbar = Hotbar.hotbarRegistry.get(player.getUUID() + "_" + type.getName());

            //Check if the Hotbar is null.
            if (hotbar == null) {

                //Get the Hotbar from the file.
                hotbar = HotbarHelper.getHotbar(player.getUUID(), type);

                //Check if the Hotbar is not null.
                if (hotbar != null) {

                    //Register the Hotbar.
                    Hotbar.hotbarRegistry.register(hotbar);
                } else {

                    //If the Hotbar is null, create a new one.
                    Hotbar.hotbarRegistry.register(new Hotbar(type, type.getItems(), player.getUUID()));
                }
            }
        }

        //Get the toolbar from the registry and set it for the player.
        Hotbar.hotbarRegistry.get(player.getUUID() + "_" + HotbarType.TOOLBAR.getName()).set(player.getPlayer());

        //Give the Hotbar switcher to the player.
        player.getPlayer().getInventory().setItem(8, Items.BAR_SWITCH);

        //Update the player's inventory.
        player.getPlayer().updateInventory();

        //Check if the player does not have a registered exp field.
        if (player.get(Fields.EXPERIENCE) == null) {

            //Make a new exp field and give the player 10 exp.
            player.set(Fields.EXPERIENCE, String.valueOf(10));
        }

        //Check if the player does not have a registered rank field.
        if (player.get(Fields.RANK) == null) {

            //Make a new exp field and give the player the default rank.
            player.set(Fields.RANK, String.valueOf(1));
        }
    }
}
