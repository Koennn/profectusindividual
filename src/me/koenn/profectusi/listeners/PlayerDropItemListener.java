package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.events.ItemInteractEvent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class PlayerDropItemListener implements Listener {

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        //Make local variables.
        CPlayer player = CPlayerRegistry.getCPlayer(event.getPlayer().getUniqueId());
        ItemStack dropped = event.getItemDrop().getItemStack();

        //Cancel the event if the player is not in creative mode.
        if (!player.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            event.setCancelled(true);
        }

        //Make the ItemInteractEvent.
        ItemInteractEvent interactEvent = new ItemInteractEvent(dropped, player.getPlayer());

        //Call the ItemInteractEvent.
        Bukkit.getPluginManager().callEvent(interactEvent);
    }
}
