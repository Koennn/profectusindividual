package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.hotbar.Hotbar;
import me.koenn.profectusi.hotbar.HotbarType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        //Make local variables.
        CPlayer player = CPlayerRegistry.getCPlayer(event.getPlayer().getUniqueId());
        Inventory inventory = player.getPlayer().getInventory();

        //Get the HotbarType that's currently selected.
        String[] split = player.getPlayer().getInventory().getItem(8).getItemMeta().getLore().get(0).split(" ");
        HotbarType type = HotbarType.valueOf(split[split.length - 1].toUpperCase());
        if (type.equals(HotbarType.TOOLBAR)) {
            type = HotbarType.WEAPONBAR;
        } else {
            type = HotbarType.TOOLBAR;
        }

        //Get the Hotbar that's currently selected.
        Hotbar hotbar = Hotbar.hotbarRegistry.get(player.getUUID() + "_" + type.getName());

        //Save the current items to the Hotbar.
        for (int i = 0; i < 8; i++) {
            ItemStack itemStack = inventory.getItem(i);
            hotbar.getItems()[i] = itemStack;
        }
    }
}
