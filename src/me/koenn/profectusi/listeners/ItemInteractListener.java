package me.koenn.profectusi.listeners;

import me.koenn.core.gui.Gui;
import me.koenn.profectusi.ProfectusIndividual;
import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.backpack.BackPackGui;
import me.koenn.profectusi.events.ItemInteractEvent;
import me.koenn.profectusi.hotbar.Hotbar;
import me.koenn.profectusi.hotbar.HotbarType;
import me.koenn.profectusi.quickshop.QuickShopGui;
import me.koenn.profectusi.references.Items;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class ItemInteractListener implements Listener {

    @EventHandler
    public void onItemInteract(ItemInteractEvent event) {
        //Make local variables.
        ItemStack item = event.getItem();
        Player player = event.getPlayer();

        //Check if the item is a BackPack.
        if (item.getType().equals(Material.CHEST) && item.getItemMeta().hasDisplayName() &&
                item.getItemMeta().getDisplayName().contains(Items.BACKPACK.getItemMeta().getDisplayName())) {

            //Cancel the event.
            event.setCancelled(true);

            //Get the player's BackPack from the registry.
            BackPack backPack = BackPack.backPackRegistry.get(player.getUniqueId().toString());

            //Close the player's current inventory.
            player.closeInventory();

            //Make the BackPackGui for the BackPack and open it.
            new BackPackGui(backPack).open(player);
        }

        //Check if the item is a hotbar switch.
        if (item.getType().equals(Material.WATCH) && item.getItemMeta().hasDisplayName() &&
                item.getItemMeta().getDisplayName().contains(Items.BAR_SWITCH.getItemMeta().getDisplayName())) {

            //Cancel the event.
            event.setCancelled(true);

            //Get the HotbarType to switch to.
            String[] split = item.getItemMeta().getLore().get(0).split(" ");
            HotbarType type = HotbarType.valueOf(split[split.length - 1].toUpperCase());

            //Get the current Hotbar.
            HotbarType current;
            if (type.equals(HotbarType.TOOLBAR)) {
                current = HotbarType.WEAPONBAR;
            } else {
                current = HotbarType.TOOLBAR;
            }

            //Save the current items to the current Hotbar.
            Hotbar currentHotbar = Hotbar.hotbarRegistry.get(player.getUniqueId() + "_" + current.getName());
            for (int i = 0; i < 8; i++) {
                ItemStack itemStack = player.getInventory().getItem(i);
                currentHotbar.getItems()[i] = itemStack;
            }

            //Get the hotbar to switch to.
            Hotbar hotbar = Hotbar.hotbarRegistry.get(player.getUniqueId() + "_" + type.getName());

            //Set the hotbar for the player.
            hotbar.set(player);

            Bukkit.getScheduler().scheduleSyncDelayedTask(ProfectusIndividual.getInstance(), () -> {
                if (type.equals(HotbarType.TOOLBAR)) {
                    player.getInventory().setItem(8, Items.BAR_SWITCH);
                } else {
                    player.getInventory().setItem(8, Items.BAR_SWITCH2);
                }
            });
        }

        //Check if the item is a quickshop item.
        if (item.getType().equals(Material.EMERALD) && item.getItemMeta().hasDisplayName() &&
                item.getItemMeta().getDisplayName().contains(Items.QUICKSHOP.getItemMeta().getDisplayName())) {

            //Cancel the event.
            event.setCancelled(true);

            //Close the player's current inventory.
            player.closeInventory();

            //Open the QuickShopGui
            Gui gui = new QuickShopGui(player);
            Gui.registerGui(gui, ProfectusIndividual.getInstance());
            gui.open();
        }
    }
}
