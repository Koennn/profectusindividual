package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.events.ItemInteractEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class PlayerInteractListener implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        //Make local variables.
        ItemStack clicked = event.getPlayer().getItemInHand();
        Block clickedBlock = event.getClickedBlock();
        CPlayer player = CPlayerRegistry.getCPlayer(event.getPlayer().getUniqueId());

        //Make sure the Action is a click action.
        if (!event.getAction().toString().contains("CLICK")) {
            return;
        }

        //Check if the item is a BackPack.
        if (clickedBlock != null && !clickedBlock.getType().equals(Material.AIR) &&
                clicked.getType().equals(Material.CHEST) && clicked.getItemMeta().hasDisplayName() &&
                clicked.getItemMeta().getDisplayName().contains(ChatColor.WHITE + "BackPack")) {

            //Check if the clicked block is a furnace.
            if (clickedBlock.getType().equals(Material.FURNACE) || clickedBlock.getType().equals(Material.BURNING_FURNACE)) {

                //Make the furnace variable.
                Furnace furnace = (Furnace) clickedBlock.getState();

                //Get the player's BackPack from the registry.
                BackPack backPack = BackPack.backPackRegistry.get(player.getUUID().toString());

                //Make local variables.
                int used;
                int removed;
                int coal = backPack.getItems()[9];

                //Check if there's coal in the BackPack
                if (coal > 0) {

                    //Get the current amount of fuel in furnace.
                    int currentFuel = 0;
                    if (furnace.getInventory().getFuel() != null && furnace.getInventory().getFuel().getType().equals(Material.COAL)) {
                        currentFuel = furnace.getInventory().getFuel().getAmount();
                    }

                    //Check if the amount of coal in the BackPack is at least a stack.
                    if (coal >= 64) {

                        //Calculate the amount of fuel needed.
                        used = 64;
                        removed = 64 - currentFuel;
                    } else {

                        //Calculate the amount of fuel needed
                        used = currentFuel + coal;
                        if (used >= 64) {
                            removed = coal - (used - 64);
                            used = 64;
                        } else {
                            removed = coal;
                        }
                    }

                    //Put the fuel in the furnace
                    furnace.getInventory().setFuel(new ItemStack(Material.COAL, used));

                    //Remove the fuel from the backpack.
                    backPack.getItems()[9] -= removed;

                    //Cancel the event if any fuel got removed.
                    if (removed > 0) {
                        event.setCancelled(true);
                    }
                    return;
                }
            }
        }

        //Make the ItemInteractEvent.
        ItemInteractEvent interactEvent = new ItemInteractEvent(clicked, player.getPlayer());

        //Call the ItemInteractEvent.
        Bukkit.getPluginManager().callEvent(interactEvent);

        //Check if the ItemInteractEvent is cancelled.
        if (interactEvent.isCancelled()) {

            //Cancel the event.
            event.setCancelled(true);
        }
    }
}
