package me.koenn.profectusi.listeners;

import me.koenn.core.player.CPlayer;
import me.koenn.core.player.CPlayerRegistry;
import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.events.ItemInteractEvent;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class InventoryClickListener implements Listener {

    @SuppressWarnings("deprecation")
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        //Make the Player variable.
        CPlayer player = CPlayerRegistry.getCPlayer(event.getWhoClicked().getUniqueId());
        ItemStack clicked = event.getCurrentItem();

        //Cancel the event if the player is not in creative mode.
        if (!player.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            event.setCancelled(true);
        }

        //Make sure the inventory exists.
        if (event.getClickedInventory() == null || event.getClickedInventory().getName() == null) {
            return;
        }

        //Check if the inventory name is 'BackPack'.
        if (event.getClickedInventory().getName().equals("BackPack")) {

            //Call the click function of the player's BackPack.
            BackPack.backPackRegistry.get(player.getUUID().toString()).click(event, player.getPlayer());
        }

        //Make the ItemInteractEvent.
        ItemInteractEvent interactEvent = new ItemInteractEvent(clicked, player.getPlayer());

        //Call the ItemInteractEvent.
        Bukkit.getPluginManager().callEvent(interactEvent);

        if (interactEvent.isCancelled() && !event.isCancelled()) {
            event.setCancelled(true);
        }
    }
}
