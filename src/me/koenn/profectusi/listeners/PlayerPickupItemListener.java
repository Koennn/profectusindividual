package me.koenn.profectusi.listeners;

import me.koenn.profectusi.backpack.BackPack;
import me.koenn.profectusi.references.Items;
import me.koenn.profectusi.saturation.SaturationSystem;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Copyright (C) Koenn - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
public final class PlayerPickupItemListener implements Listener {

    @EventHandler
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        //Make local variables.
        ItemStack item = event.getItem().getItemStack();
        Player player = event.getPlayer();

        //Get the player's BackPack from the registry.
        BackPack backPack = BackPack.backPackRegistry.get(player.getUniqueId().toString());

        //Loop over all BackPack items.
        for (int i = 0; i < Items.BACKPACK_ITEMS.length; i++) {

            //Check if the picked up item is the same as the BackPack item.
            if (Items.BACKPACK_ITEMS[i].equals(item.getType())) {

                //Cancel the event.
                event.setCancelled(true);

                //Remove the item from the ground.
                event.getItem().remove();

                //Add the amount of items on the ground to the amount in the BackPack.
                backPack.getItems()[i] += item.getAmount();

                //Play the item pickup sound.
                player.getWorld().playSound(player.getLocation(), Sound.ITEM_PICKUP, 0.5F, 2.0F);
            }
        }

        //Check if the item gives any saturation.
        if (SaturationSystem.givesSaturation(item)) {

            //Cancel the event.
            event.setCancelled(true);

            //Remove the item from the ground.
            event.getItem().remove();

            //Give the player the right amount of saturation.
            SaturationSystem.giveSaturation(player, SaturationSystem.getSaturation(item) * item.getAmount());

            //Play the saturation.
            player.getWorld().playSound(player.getLocation(), Sound.EAT, 0.5F, 1.0F);
        }
    }
}
